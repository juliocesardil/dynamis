<?php
class Tracer{
    public static $gestor;
    public static $nombre;
    public static $prendido = false;

    public static function iniciaRastreo($nombre){
            self::$gestor = fopen(config::$traceDir . $nombre.".log","a");
            self::$prendido = true;
    }

    public static function  escribir($nivel,$tipo,$mensaje){  
        global $v;
        if(self::$prendido){  
            $a = Tracer::getRastreo();
            $expresion = "(\s)+";//Expresion para encontrar caracteres whitespaces 
            $mensaje = preg_replace("/".$expresion."/",' ',$mensaje);//Los cambios por espacios simples, esto es para mejorar la visivilidad en el trace
            fwrite($a,"\n".str_repeat("\t",$nivel)."[".$tipo."]".$mensaje." ~".self::tiempo());
        }
    }

    public static function  debug($mensaje){  
        global $v;
        self::escribir($v->__nivelrastreo,"DEB",$mensaje);
    }
    

    public static function cerrarRastreo(){
        $archivo = Tracer::getRastreo();        
        fclose($archivo);
    }
    
    private static function getRastreo(){
        return self::$gestor;
    }

    public static function tiempo(){
        $now = DateTime::createFromFormat('U.u', microtime(true));
        return $now->format("Y-m-d H:i:s.u");
    }
 
}
?>