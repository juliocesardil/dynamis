<?php
class Database {
	public static $db;
	public static $con;
	function __construct(){
		$this->user = config::$dbconexion["user"];
		$this->pass = config::$dbconexion["pass"];
		$this->host = config::$dbconexion["host"];
		$this->ddbb = config::$dbconexion["ddbb"];
	}

	function connect(){
		try{
			return $con = new PDO("mysql:host=".$this->host.";dbname=".$this->ddbb.";charset=UTF8",$this->user,$this->pass);		
		}catch(PDOException $e){
			$resp = new Result();			
			$resp->serverError = true;
			$resp->rows= array(array("errno"=>$e->getCode(),"error"=>"Error de conexion, por favor revisa la configuración de conexion a la BD."));
			$resp->publish();
		}
	}

	public static function getCon(){
		if(self::$con==null && self::$db==null){
			self::$db = new Database();
			self::$con = self::$db->connect();
		}
		return self::$con;
	}
	
}
?>
