<?php

class Executor {

	public static function doit($sql){
		//Esta funcion era del framework anterior
		$con = Database::getCon();
		if(Core::$debug_sql){
			print "<pre>".$sql."</pre>";
		}
		return array($con->query($sql),$con->insert_id);
	}

	public static function run($sql){
		
		$con = Database::getCon();
		$resp = new Result();		

		try{
			if(config::$transac){
				if($con->beginTransaction()){
					__debug("Inicia transacción");
					__trace("WRN","Inicia transacción",1);
				}
			}
		}catch(PDOException $e){	
			if($e->getMessage() != "There is already an active transaction"){
				__trace("ERR",$e->getMessage(),1);
			}			
		}
		__debug("Ejecutando SQL");		
		__trace("SQL",$sql,1);

		if($rs = $con->query(substr($sql,1,-1))){
			$resp->affected_rows = $rs->rowCount();			
			__trace("INF","Registros afectados:".$rs->rowCount(),1);
			__debug("Registros afectados: ".$resp->affected_rows);//Debug		
			switch(gettype($rs)){
				case "boolean":
					$resp->affected_rows = $rs->rowCount();
					break;
				default:								
					$resp->affected_rows = $rs->rowCount();	
					$resp->num_rows = $rs->rowCount();
					$resp->rows = $rs->fetchAll(PDO::FETCH_ASSOC);					
					$rs->closeCursor();		
					break;
			}	
			$rs = null;		
		}else{
			if(config::$transac){
				cmd::execute("rollback");
			}
			$resp->serverError = true;
			$resp->rows = array(array("errno" =>$con->errorInfo()[1], "error" => $con->errorInfo()[2]));									
			__trace("ERR","Errno:".$resp->rows[0]["errno"].", ".$resp->rows[0]["error"],1);
			
			$resp->publish();
		}		
		return $resp;
	}
}
?>