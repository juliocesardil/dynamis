<?php
class Result{
    public $serverError;
    private $pointer;
    public $num_rows;
    public $affected_rows;
    public $rows;    

    public function __construct(){        
        $this->serverError = false;
        $this->pointer = -1;
        $this->num_rows = 0;
        $this->affected_rows = 0;
        $this->rows = null;                
    }

    public function next(){                
        if($this->pointer == ($this->num_rows - 1)){return false;}        
        $this->pointer++;
        $this->createVars();        
        return true;
    }

    private function createVars(){
        foreach($this->rows[$this->pointer] as $clave => $valor){            
            Scope::set($clave,$valor);
        }                
        __debug("Agregado a scope: ".json_encode($this->rows[$this->pointer]));
    }

    public function publish(){        
        $js = json_decode("{}");
        $js->serverError = $this->serverError;        
        $js->num_rows = $this->num_rows;
        $js->affected_rows = $this->affected_rows;
        $js->rows = json_decode(json_encode($this->rows));
        __debug("Devolviendo respuesta");
        if(config::$transac){
            cmd::execute("commit");
        }        
        __trace("INF","Devolviendo resultado");
        echo json_encode($js);
        exit;
    }

    public function acumular($rs){
        if(!$rs->serverError){
            $this->rows =array_merge((array)$this->rows, $rs->rows);
            $this->num_rows += $rs->num_rows;
            $this->affected_rows += $rs->affected_rows;            
        }
    }
}
?>