<?php
class Scope{
    public static $varsAmb = null;
    public static $scopes;    
    public static $profundidad = -1;
    public static $debug = false;

    private $nivel;
    private $variables;
    
    public function __construct(){
        //Se inicializa el scope con el objeto variables creado
        $this->variables = json_decode("{}");        
    }

    public static function actual(){
        //Devuelve el ultimo scope creado
        $a = self::$scopes[self::$profundidad];                
        return $a;
    }

    public static function anterior(){
        //Devuelve el ultimo scope creado        
        $a = self::$scopes[self::$profundidad-1];
        return $a;
    }

    //Estos metodos son para obtener acceso a los elementos desde los comandos
        //Importante el parametro $numscope apunta al scope que se desea , 
        //Valor 0 es el scope actual, este es el valor por default por que se puede omitir
        //Valor 1 para el scope superior
    //Funcion para obtener una variable del scope,
    public static function get($nombre,$numscope = 0){
        return self::$scopes[self::$profundidad - $numscope]->variables->$nombre;
    }

    //Funcion para crear o asignar un nuevo valor en el scope
    public static function set($nombre,$valor,$numscope = 0){
        self::$scopes[self::$profundidad - $numscope]->variables->$nombre = $valor;
    }

    //Funcion para checar si una variable existe en el scope
    public static function existe($nombre, $numscope = 0){
        if(property_exists(self::$scopes[self::$profundidad - $numscope]->variables, $nombre)){
            return true;
        }else{
            return false;
        }
    }   
    //-----------------------------------------------------------------------
    

    public function getVariables(){
        //Meotodo para obtener las variables del scope
        //Copia las variables de ambiente
        $sc = clone self::$varsAmb;
        //Copia las variables del scope si es que hay
        foreach($this->variables as $variable => $valor){
            $sc->$variable = $valor;
        }
        //Devuelve las variables del scope actual
        return $sc;
    }

    static function crear($varsLocales = null){               
        //Crear un nuevo scope con sus propias variables        
        $sc = new Scope();
        //Le asigno las variables locales si es que se pasaron
        if(!is_null($varsLocales)){
            foreach($varsLocales as $variable => $valor){                                
                $sc->variables->$variable = $valor;                
            }   
        }
        
        //Se aumenta la profundidad de los scope
        self::$profundidad+=1;
        //Se asigna el numero al scope actual, para que sepa que nivel es
        $sc->nivel = self::$profundidad;           
        
        __debug("Nuevo scope (".self::$profundidad."): ".json_encode($sc->variables)."");
        __trace("SCO","Nuevo scope (".self::$profundidad."): ".json_encode($sc->variables)."");
        self::$scopes[self::$profundidad] = $sc;
    }

    //Elimina el ultimo scope creado
    static function eliminar(){       
        __debug("Eliminando scope (".self::$profundidad.")");
        __debug("SCO","Eliminando scope (".self::$profundidad.")");
        unset(self::$scopes[self::$profundidad]);
        self::$profundidad--;
        
    }

    static function defVarAmbiente($nombre,$valor){        
        if(strlen(trim($nombre))<=0){
            return false;
        }
        $nombre = "__".trim($nombre);
        if(is_null(self::$varsAmb)){
            self::$varsAmb = json_decode("{}");
        }
        self::$varsAmb->$nombre = $valor;        
        return true;
    }
}
?>