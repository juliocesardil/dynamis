<?php
//Funcion para tener output en pantalla sirve para debugear
function __debug($m){
    if(Scope::$debug){
        echo "<br>".Scope::$profundidad."".str_repeat("   ",Scope::$profundidad). $m;        
    }    
}

//Funcion que ayuda con las llamadas del trace
function __trace($tipo,$msj,$num = 0){
    Tracer::escribir(Scope::$profundidad + $num,$tipo,$msj); //Rastreo
}

?>