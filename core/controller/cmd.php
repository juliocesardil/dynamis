<?php  
class cmd extends parser{

    public static function execute($cmd){
		$cmd = self::cleanCmd($cmd);					
		if(self::isSQL($cmd)){			
			__debug("***SQL: ".$cmd);
			$obj = self::runSQL($cmd);
		}else{			
			__debug("***CMD: ".$cmd);
			$obj = self::runCMDJ($cmd);
		}		
		return $obj;
	}

	private static function runCMDJ($cmd){				
		__trace("CMD",$cmd);
		$var = null;//variables a crar en el nuevo scope
		$cmdname = self::getCmdj($cmd);
		$varstring = self::getStrVars($cmd);
		if($varstring != null){
			//Solo si el comando tiene parametros 						
			//__debug("Parametros: ".$varstring);			
			//__trace("INF","Parametros ".$varstring,1);
			$newvarstring = self::parse($varstring);			
			//__debug("Reemplazo: ".$newvarstring);
			$var = self::createVars($newvarstring);			
			//__debug("Nuevas variables: ".json_encode($var));			
			__trace("INF","Reemplazo ".$newvarstring,1);		
		}
		Scope::crear($var); //Crear scope para el comando que se cargará
		$respuesta = self::loadCommand($cmdname);
		Scope::eliminar(); //Después de la ejecucion del comando se elimina el scope
		return $respuesta;
	}

	private static function runSQL($sql){		
		__trace("SQJ",substr($sql,1,-1));
		$puresql = self::parse($sql);		
		__debug("Reemplazo: ".$puresql);		
		return self::exeSQL($puresql);
	}
    
	private static function loadCommand($cmd){		
		$found = false;
		$lvl =  count(config::$cmdpath)-1;
		do{			
			$fullpath = config::$cmdpath[$lvl]["path"].$cmd.".". config::$cmdext;			
			if(file_exists($fullpath)){
				$found = true;				
				__trace("INF","Comando encontrado en el nivel ".config::$cmdpath[$lvl]["name"]." (".$lvl.")");												
				$resultado =  include $fullpath;								
				return $resultado;				
			}else{
				$lvl-=1;
			}
		}while($found == false and $lvl >= 0);
		if($found == false){			
			__trace("ERR","No se encontró el comando (".$cmd.")");
			if(config::$transac){cmd::execute("rollback");}
			return self::execute("devolver mensaje where cvemsj = 'E10000' and cmdj = \"$cmd\"")->publish();
		}		
	}

	private static function createVars($StrVars){
		//__debug("Creando objeto variables");
		$vars = json_decode("{}");
		$arrayvars = explode(" and ",$StrVars);
		foreach($arrayvars as $var){
			$ss = "\$vars->" . $var .";";
			try{
				eval($ss);
			}catch(ParseError $e){
				__trace("ERROR","Error sintanxis incorrecta");
				$error = new Result();
				$error->serverError = true;				
				$error->rows = array(array(
										"cvemsj" => "ERROR",
										"mensaje" => "Error de sintanxis."
									));
				$error->publish();
			}			
		}	
		return $vars;
	}

	private static function exeSQL($sql){
		return Executor::run($sql);
	}

	private static function cleanCmd($dirtyCmd){
		//Es necesario quitar saltos de linea, espacios de sobra, tabulaciones que no sean parte del contenido de variables
		$expresion = "(\s)+(?=(?:[^']*'[^']*')*[^']*$)";
		$cleanCmd = $string = preg_replace("/".$expresion."/",' ',$dirtyCmd);
		return $cleanCmd;
	}
}
?>