<?php
class config{ 
    
    
    //Nombre y version de la instancia    
    public static $nombre = "Dynamis Versión 1.3";
    //******* Version 1.3
    //Se agregó el manejo de permisos, se crearon tablas y se crearon comandos

    //******* Version 1.2.1
    //Se agregó la captura de error de sintaxis al parsear variables
    //Se agregó método acumular para poder acumular registros de varios comandos
    //Validación de tipo de dato en el comando verifica variable

    //******* Version 1.2
    //Se agrego la clase Scope    
    //Manejo de scopes por comandos, permitiendo un mejor parseo de variables
    //Cada comando o sentencia sql es parseada con el scope superior inmediato    

    //******* Version 1.1
    //Ahora se manejan comandos y sentencias sql con saltos de linea 
    //Se mejoró el parseo de variables
    //Ya no se hace reemplazo de variables dentro de los valores de textos.
    //Se agregó el manejo de errores en la base de datos
    //Se corrigió la escritura en el trace cuando se enviaban saltos de linea
    


    /*
    Configuracion para la conexion a la base de datos
    */   
    public static $dbconexion = array(
        "user" => "root",
        "pass" => "",
        "host" => "localhost",
        "ddbb" => "minus",
        "port" => ""        
    );

    /*
    Permite configurar los niveles de comandos, el nivel core nunca se debe eliminar
    Para agregar otro nivel se debe agregar al final del array, campos necesario son path, name y nivel
    */   
    public static $cmdpath = array(
        array(
            "name" => "CORE",
            "nivel" => 0,
            "path" => "core/app/commands/core/",            
            "desc" => "Ruta de comandos del sistema." ),
        array(
            "name" => "PERMISOS",
            "nivel" => 100,
            "path" => "core/app/commands/acceso/",
            "desc" => "Comandos para manejo de permisos")
    );
    /*
    Ruta donde se almacenan los archivos de rastreo, este se recomienda ponerlo fuera de la carpeta 
    de publicacion para evitar sea visualizado directamente en el explorardor   
    */
    public static $traceDir = "C:\\ver\\";

    /*
    Configura la extension de los comandosj
    */
    public static $cmdext = "cmdj";

    /*
    Permite cambiar el largo del id de sesiones
    */
    public static $lensess = 20;    

    /*
    Idioma predeterminado
    */
    public static $idioma = "SPA";

    /*
    Tiempo de vida de las sesiones en minutos
    Asingar cero para desactivar
     */
    public static $tiempoMaxSesion = 0;

    /*
    Activa y desactiva la capacidad de hacer realizar los procesos por transaccion
    */
    public static $transac = false;

}
?>