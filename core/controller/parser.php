<?php
class parser{

    private static $varchar = '@';

    public static function parse($string){        
        $scope = Scope::actual()->getVariables();        
        $StrOut = $string;
        $charvar = self::$varchar;        
        foreach($scope as $variable => $valor){
            $StrOut = self::enviromentVar($StrOut, $variable,$valor);
            $StrOut = self::normalVar($StrOut, $variable,$valor);
            $StrOut = self::optionalVar($StrOut, $variable,$valor);
            $StrOut = self::optionalTableVar($StrOut, $variable,$valor);
            $StrOut = self::likeVar($StrOut, $variable,$valor);
            $StrOut = self::likeTableVar($StrOut, $variable,$valor);            
        }
        $StrOut = self::cleanVars($StrOut);                
        return $StrOut;
    }

    private static function enviromentVar($string, $nombrevar, $valor){
        //Reemplazar la variable de ambiente de la forma @@variable        
        //las variables de entorno empiezan por doble guin bajo en el scope, verifico si la variable proporcionada es de entorno
        if(preg_match_all('/^(__)/',$nombrevar) > 0){ 
            //Las variables en el scope tiene __variable, tengo que cambiar para que los guiones sean reemplazados con el charvar y la variable quede @@varenviroment
            $nombrevar = preg_replace('/^(__)/','',$nombrevar);
            //Creo la expresion para encontrar la variables de entorno en el comando, este solo reemplaza las variables que no estan entre comillas simples
            $expresion = self::$varchar."{2}".$nombrevar."\b(?=(?:[^']*'[^']*')*[^']*$)"; 
            //Aplico el reemplazo segun el tipo de valor de la variable;
            switch(gettype($valor)){
                case "boolean":
                case "integer":
                case "double":                
                    $string = preg_replace("/".$expresion."/i",$valor,$string); 
                    break;
                case "NULL":  
                    $string = preg_replace("/".$expresion."/i","NULL",$string); 
                    break;
                case "string":
                    $string = preg_replace("/".$expresion."/i",'\''.$valor.'\'',$string);
                    break;
            }
        }
        //Se devuelve la cadena con el reemplazo de variables         
        return $string;
    }

    private static function normalVar($string, $nombrevar, $valor){        
        //Reemplazar la variable simple de la forma @variable        
        //Creo la expresion para encontrar la variables de entorno en el comando, este solo reemplaza las variables que no estan entre comillas simples
        $expresion = self::$varchar."{1}".$nombrevar."\b(?=(?:[^']*'[^']*')*[^']*$)"; 
        //Aplico el reemplazo segun el tipo de valor de la variable;
        switch(gettype($valor)){
            case "boolean":
            case "integer":
            case "double":
                $string = preg_replace("/".$expresion."/i",$valor,$string); 
                break;
            case "NULL":  
                $string = preg_replace("/".$expresion."/i","NULL",$string); 
                break;
            case "string":
                $string = preg_replace("/".$expresion."/i",'\''.$valor.'\'',$string);
                break;
        }
        //Se devuelve la cadena con el reemplazo de variables         
        return $string;
    }

    private static function optionalVar($string, $nombrevar, $valor){        
        //Reemplazar la variable simple de la forma @+variable        
        //Creo la expresion para encontrar la variables de entorno en el comando, este solo reemplaza las variables que no estan entre comillas simples
        $expresion = self::$varchar."{1}\+".$nombrevar."\b(?=(?:[^']*'[^']*')*[^']*$)"; 
        //Aplico el reemplazo segun el tipo de valor de la variable;
        switch(gettype($valor)){
            case "boolean":
            case "integer":
            case "double":
                $string = preg_replace("/".$expresion."/i",$nombrevar.' = '.$valor,$string); 
                break;
            case "NULL":  
                $string = preg_replace("/".$expresion."/i",$nombrevar.' is NULL',$string); 
                break;
            case "string":
                $string = preg_replace("/".$expresion."/i",$nombrevar.' = \''.$valor.'\'',$string);
                break;
        }
        //Se devuelve la cadena con el reemplazo de variables         
        return $string;
    }

    private static function optionalTableVar($string, $nombrevar, $valor){        
        //Reemplazar la variable simple de la forma @+tabla.variable        
        //Armo la expresion para buscar variables considerando que tienen nombres de tablas, siempre y cuando no esten entre comillas simples
        $expre = self::$varchar."{1}\+\w+\.".$nombrevar."\b(?=(?:[^']*'[^']*')*[^']*$)";
        //Busco las coincidencias
        preg_match_all("/".$expre."/i", $string, $coincidencias);
        //Se hace for each porque una variable puede estar con diferentes tablas
        foreach($coincidencias[0] as $tablavar){
            //Armo una presion pero ahora para buscar la variable con una tabla en especifico, siempre y cuando no esten entre comillas simples            
            //Se se escapan el nombre de la variable para envitar conflicto con el punto(.) y el más(+) en en la regex      
            $tablavaresc = preg_quote($tablavar); 
            //Se crea la expresion de busqueda con los caracteres escapados
            $expresion = $tablavaresc."\b(?=(?:[^']*'[^']*')*[^']*$)";            
            //Aplico el reemplazo segun el tipo de valor de la variable;
            switch(gettype($valor)){
                case "boolean":
                case "integer":
                case "double":
                    $string = preg_replace("/".$expresion."/i",substr($tablavar, 2)." = ".$valor,$string); 
                    break;            
                case "NULL":
                    //Se usa substr debido a que $tablavar cotiene @+tabla.variable y con esto se quitan los primeros dos caracteres
                    $string = preg_replace("/".$expresion."/i",substr($tablavar, 2)." is NULL",$string); 
                    break;
                case "string":
                    $string = preg_replace("/".$expresion."/i",substr($tablavar, 2)." = '".$valor."'",$string);                     
                    break;
            }
        }
        //Se devuelve la cadena con el reemplazo de variables         
        return $string;
    }

    private static function likeVar($string, $nombrevar, $valor){        
        //Reemplazar la variable simple de la forma @%variable        
        //Creo la expresion para encontrar la variables de entorno en el comando, este solo reemplaza las variables que no estan entre comillas simples
        $expresion = self::$varchar."{1}%".$nombrevar."\b(?=(?:[^']*'[^']*')*[^']*$)"; 
        //Aplico el reemplazo segun el tipo de valor de la variable;
        switch(gettype($valor)){
            case "boolean":
            case "integer":
            case "double":
            case "NULL":  
            case "string":
                $string = preg_replace("/".$expresion."/i",$nombrevar.' like \''.$valor.'\'',$string);
                break;
        }
        //Se devuelve la cadena con el reemplazo de variables         
        return $string;
    }

    private static function likeTableVar($string, $nombrevar, $valor){        
        //Reemplazar la variable simple de la forma @+tabla.variable        
        //Armo la expresion para buscar variables considerando que tienen nombres de tablas, siempre y cuando no esten entre comillas simples
        $expre = self::$varchar."{1}%\w+\.".$nombrevar."\b(?=(?:[^']*'[^']*')*[^']*$)";
        //Busco las coincidencias
        preg_match_all("/".$expre."/i", $string, $coincidencias);
        //Se hace for each porque una variable puede estar con diferentes tablas
        foreach($coincidencias[0] as $tablavar){
            //Armo una presion pero ahora para buscar la variable con una tabla en especifico, siempre y cuando no esten entre comillas simples            
            //Se se escapan el nombre de la variable para envitar conflicto con el punto(.) y el más(+) en en la regex      
            $tablavaresc = preg_quote($tablavar); 
            //Se crea la expresion de busqueda con los caracteres escapados
            $expresion = $tablavaresc."\b(?=(?:[^']*'[^']*')*[^']*$)";            
            //Aplico el reemplazo segun el tipo de valor de la variable;
            switch(gettype($valor)){
                case "boolean":
                case "integer":
                case "double":
                case "NULL":
                break;
                case "string":
                    $string = preg_replace("/".$expresion."/i",substr($tablavar, 2)." like '".$valor."'",$string);                     
                    break;
            }
        }
        //Se devuelve la cadena con el reemplazo de variables         
        return $string;
    }

    private static function cleanVars($string){
        //Limpia las variables inexistentes
        //No lo pongo en unbucle para ver como es el reemplazo de cada tipo de variable 
        $expre = self::$varchar."{1}\+\w+\.\w+\b(?=(?:[^']*'[^']*')*[^']*$)";
        $string = preg_replace("/".$expre."/i", '1 = 1', $string);  //quita las variables de la forma @+tabla.campo
        $expre = self::$varchar."{1}\+\w+\b(?=(?:[^']*'[^']*')*[^']*$)";
        $string = preg_replace("/".$expre."/i", '1 = 1', $string);  //quita las variables de la forma @+campo
        $expre = self::$varchar."{1}%\w+\.\w+\b(?=(?:[^']*'[^']*')*[^']*$)";
        $string = preg_replace("/".$expre."/i", '1 = 1', $string);  //quita las variables de la forma @%tabla.campo
        $expre = self::$varchar."{1}%\w+\b(?=(?:[^']*'[^']*')*[^']*$)";
        $string = preg_replace("/".$expre."/i", '1 = 1', $string);  //quita las variables de la forma @%campo
        $expre = self::$varchar."{2}\w+\b(?=(?:[^']*'[^']*')*[^']*$)";
        $string = preg_replace("/".$expre."/i", 'NULL', $string);  //quita las variables de la forma @@campo
        $expre = self::$varchar."{1}\w+\b(?=(?:[^']*'[^']*')*[^']*$)";
        $string = preg_replace("/".$expre."/i", 'NULL', $string);  //quita las variables de la forma @campo
        return $string;
    }

    
    public static function isSQL($cmd){        
        if(preg_match('/^\[/',$cmd)){
            return true;
        }else{
            return false;
        }
    }

    public static function getCmdj($cmd){
        $a = preg_replace('/( where.*)/i','',$cmd);
        $a = preg_replace('/\s+/','_',$a);
        $a = strtolower($a);
        return $a;
    }

    public static function getStrVars($cmd){
        //echo "<br><br>".$cmd;
        preg_match('/( where.*)/i',$cmd,$a);
        if(count($a) > 0){
            //Si se encontraron parametros devuelve el string con los parametros
            $a = preg_replace('/^( where )/i','',$a[0]);
        }else{
            //Si no hay parametros devuelve null
            $a = null;
        }        
        return $a;
    }

}
?>
