<?php	
	$data = file_get_contents("php://input");

	if(isset($data)){				
		if(is_null($request = json_decode($data))){
			//En caso que la peticion traiga un mal formato
			//y devuelva error al intentar convertir a json
		}
	}

	//Pruebas de peticiones
	//include "res/test/prueba_logeo.php";	

	//Las variables que se deben recibir en la peticion
	//-Se deben de recibir aunque sea vacias, a exception de las obligatorias* como cmdj
	if(!isset($request) or 
		!isset($request->tipo) or //*login|request
		!isset($request->usuario) or //si es una peticion de tipo login dejar vacio
		!isset($request->sesion) or //si es una peticion de tipo login dejar vacio
		!isset($request->cmdj) or //*comando a ejecutar en el server, nunca debe estar vacio
		!isset($request->rastreo) //*nombre del archivo del trace si es que se prendio sino dejar vacio		
		){
		echo config::$nombre; //Manda el nombre de la instancia si se recibio una peticion erronea
		exit;
	}
	
	//Scope::$debug = true;
	
	//Rastreo
	if(isset($request->rastreo) and strlen(trim($request->rastreo)) > 0){
		Tracer::iniciaRastreo(trim($request->rastreo));
	}
	
	Scope::crear();
	Scope::defVarAmbiente("idioma",config::$idioma);
	
	if($request->tipo == "login"){		
		cmd::execute("iniciar sesion where ". $request->cmdj)->publish();
	}else{
		Scope::defVarAmbiente("sesion",$request->sesion);
		Scope::defVarAmbiente("usuario",$request->usuario);
		cmd::execute("verificar sesion");				
	}
	//Si todo ha salido bien ejecuta el comando recibido
	cmd::execute($request->cmdj)->publish();
?>